from dateparser import parse

import re
from datetime import datetime

from .const import (
        SECTION_WEEKS, COLUMN_WEEK, COLUMN_DUE_DATE, COLUMN_WEEK_START,
        SECTION_COURSES, TABLES_KEY, NO_TASK_STRINGS, COMPLETE_TASK_STRINGS, TIMEDELTA_WEEK_LENGTH,
        UNFINISHED_TASK_STRINGS, URGENCY_LEVEL_CHILL, URGENCY_LEVELS)

class PlanSchemeException(Exception):
    """Raised when a study plan does not follow the standard."""

# Parsing raw markdown into comprehensible text

class Header:
    def __init__(self, level, text):
        self.level = level
        self.text = text

    def __repr__(self):
        return f"Header({self.level}, '{self.text}')"

class Table:
    def __init__(self, header, columns=None):
        self._header = header
        if columns is None:
            self._columns = {
                head: []
                for head in header
            }
        else:
            self._columns = columns

    def append(self, row):
        if len(row) != len(self._header):
            raise PlanSchemeException("Row {} in table {} has wrong number of columns".format(row, self._header))
        for head, entry in zip(self._header, row):
            self._columns[head].append(entry)

    def __getitem__(self, key):
        return self._columns[key]

    def __contains__(self, key):
        return key in self._columns

    def __repr__(self):
        return f'Table({self._header}, {self._columns})'

    def __eq__(self, other):
        if isinstance(other, Table):
            return self._header == other._header and self._columns == other._columns
        return False

def _extract_nonempty_group(match):
    for t in match.groups():
        if t is not None:
            return t
    return ''

def _extract_only_group(match):
    return match.groups()[0]

REGEX_ITALICS_BOLD_MATCH_SYMBOLS = r"\*\*(\w.*)\*\*|\*(\w.*)\*|\_\_(\w.*)\_\_|\_(\w.*)\_"
REGEX_LINK = r"\[(.+)\]\(.*\)"

def markdown_to_plaintext(markdown):
    no_italics_bold = re.sub(REGEX_ITALICS_BOLD_MATCH_SYMBOLS, _extract_nonempty_group, markdown)
    no_links = re.sub(REGEX_LINK, _extract_only_group, no_italics_bold)
    return no_links

REGEX_HEADER = r"^(\#+)(.*)$"

def extract_header(line):
    match = re.match(REGEX_HEADER, line)
    if match:
        hashes, text = match.groups()
        text = markdown_to_plaintext(text)
        level = len(hashes)
        text = text.strip()
        return True, level, text
    return False, None, None

REGEX_TABLE_FILLER_ROW = r"^\|[-|]*\|\s*$"
REGEX_TABLE = r"^\|(.*)\|\s*$"
REGEX_EMOJIS = r":[a-zA-Z0-9_-]+:"

def _clean_entry_text(entry_str):
    stripped = entry_str.strip()
    return stripped

def extract_table_line(line):
    match = re.match(REGEX_TABLE_FILLER_ROW, line)
    if match:
        return True, None
    match = re.match(REGEX_TABLE, line)
    if match:
        inner_part = match.groups()[0]
        entries = [_clean_entry_text(str_) for str_ in inner_part.split('|')]
        return True, entries
    return False, None

def parse_to_entries(markdown):
    entries = []
    in_table = False
    for line in markdown.splitlines():
        if in_table:
            is_table, row_entries = extract_table_line(line)
            if is_table:
                if row_entries is not None:
                    entries[-1].append(row_entries)
                continue
            else:
                in_table = False
        is_header, level, text = extract_header(line)
        if is_header:
            entries.append(Header(level, text))
            continue
        is_table, row_entries = extract_table_line(line)
        if is_table:
            if row_entries is not None:
                entries.append(Table(row_entries))
            in_table = True
            continue
    return entries

# Aggregateing parsed entries into a coherent plan

def week_table_to_dict(table):
    """
    Maps week numbers to start dates.
    """
    return dict(zip(table['Week'], map(parse, table['Monday'])))

def _entries_to_tree(entries, level=0):
    """Returns nested dictionaries where inner nodes are sections and leaf
    nodes are tables.
    """
    tree = {}
    while len(entries) > 0:
        if isinstance(entries[0], Header):
            if entries[0].level <= level:
                return tree, entries
            else:
                tree[entries[0].text], entries = _entries_to_tree(entries[1:], level=entries[0].level)
        elif isinstance(entries[0], Table):
            if TABLES_KEY not in tree:
                tree[TABLES_KEY] = []
            tree[TABLES_KEY].append(entries[0])
            entries = entries[1:]
    return tree, entries

def entries_to_tree(entries):
    tree, _ = _entries_to_tree(entries)
    return tree

def _only_element_of_tree(tree):
    """If the dictionary has only one element, returns the element."""
    if len(tree) == 1:
        return next(iter(tree.values()))
    else:
        if len(tree) != 0:
            raise PlanSchemeException(
                "Given plan does not follow scheme: More than one top level header")
        else:
            raise PlanSchemeException(
                "Given plan does not follow scheme: No headers given")

def _weeks_table_to_dict(weeks_table):
    weeks = {
        int(week): parse(monday_str) + TIMEDELTA_WEEK_LENGTH for week, monday_str
        in zip(weeks_table._columns[COLUMN_WEEK],
            weeks_table._columns[COLUMN_WEEK_START])
    }
    return weeks

def _course_table_to_tasks(course_table, weeks):
    if COLUMN_DUE_DATE in course_table and COLUMN_WEEK in course_table:
        raise PlanSchemeException(f"Both '{COLUMN_DUE_DATE}' and '{COLUMN_WEEK}' in table.")
    tasks = []
    if COLUMN_DUE_DATE in course_table:
        for i in range(len(course_table[COLUMN_DUE_DATE])):
            due_dict = {}
            for task in course_table._header:
                if task == COLUMN_DUE_DATE:
                    continue
                if course_table[task][i] not in NO_TASK_STRINGS:
                    due_dict[task] = course_table[task][i]
            due_date = parse(course_table[COLUMN_DUE_DATE][i])
            if len(due_dict) > 0:
                task = (due_date, due_dict)
                tasks.append(task)
    elif COLUMN_WEEK in course_table:
        for week_col_i in range(len(course_table._columns[COLUMN_WEEK])):
            week_num_str = course_table[COLUMN_WEEK][week_col_i]
            week_num = int(week_num_str)
            week_dict = {}
            for task in course_table._header:
                task_text = course_table[task][week_col_i]
                if task == COLUMN_WEEK:
                    continue
                if task_text not in NO_TASK_STRINGS:
                    week_dict[task] = task_text
            if week_num not in weeks:
                raise PlanSchemeException(
                        f"Table has week {week_num} but it hasn't been defined in the weeks table.")
            due_date = weeks[week_num]
            if len(week_dict) > 0:
                task = (due_date, week_dict)
                tasks.append(task)
    else:
        raise PlanSchemeException(f"Neither '{COLUMN_DUE_DATE}' nor '{COLUMN_WEEK}' in table.")
    return tasks

class Plan:
    def __init__(self):
        self.weeks = {}
        self.courses = {}

    @classmethod
    def from_entries(cls, entries):
        plan = cls()
        tree = entries_to_tree(entries)
        root = _only_element_of_tree(tree)
        if SECTION_WEEKS not in root:
            raise PlanSchemeException(f"No section '{SECTION_WEEKS}'")
        if TABLES_KEY not in root[SECTION_WEEKS]:
            raise PlanSchemeException(f"No table in section '{SECTION_WEEKS}'")
        if len(root[SECTION_WEEKS][TABLES_KEY]) > 1:
            raise PlanSchemeException(f"More than 1 table in section '{SECTION_WEEKS}'")
        weeks_table = root[SECTION_WEEKS][TABLES_KEY][0]
        if COLUMN_WEEK not in weeks_table:
            raise PlanSchemeException(f"{COLUMN_WEEK} not in Weeks table")
        if COLUMN_WEEK_START not in weeks_table:
            raise PlanSchemeException(f"{COLUMN_WEEK_START} not in Weeks table")
        weeks = _weeks_table_to_dict(weeks_table)
        if SECTION_COURSES not in root:
            raise PlanSchemeException(f"No section '{SECTION_COURSES}'")
        courses = {}
        for course, dict_ in root[SECTION_COURSES].items():
            if TABLES_KEY not in dict_:
                raise PlanSchemeException(f"No table for course '{course}'")
            course_tasks = []
            for table in dict_[TABLES_KEY]:
                course_tasks.extend(_course_table_to_tasks(table, weeks))
            courses[course] = course_tasks
        plan.weeks = weeks
        plan.courses = courses
        return plan
        

def _sort_todo_list(todo):
    return sorted(todo, key=lambda x: x[0])


def _remove_emojis(entry_str):
    no_emojis = re.sub(REGEX_EMOJIS, '', entry_str).strip()
    return no_emojis


def courses_to_todo(courses):
    # will be of form (due_date, course_name, task_type, task_text)
    todo = []
    # list of tasks of form (due_date, due_dict)
    for course, tasks in courses.items():
        for due_date, due_dict in tasks:
            unfinished_dict = {}
            for task_type, task_text in due_dict.items():
                if not any(str_ in task_text for str_ in COMPLETE_TASK_STRINGS):
                    # it's kind of dirty to do this here, but eh I guess it
                    # works
                    task_text = _remove_emojis(task_text)
                    unfinished_dict[task_type] = task_text
                    todo_entry = (due_date, course, task_type, task_text)
                    todo.append(todo_entry)
    todo = _sort_todo_list(todo)
    return todo

def plan_to_todo(plan):
    return courses_to_todo(plan.courses)

def datetime_urgency(datetime_):
    now = datetime.now()
    if datetime_ > now + URGENCY_LEVELS[0][2]:
        return URGENCY_LEVEL_CHILL[0], URGENCY_LEVEL_CHILL[1]
    for level, urgency_name, urgency_timedelta in reversed(URGENCY_LEVELS):
        if datetime_ <= now + urgency_timedelta:
            return level, urgency_name
    assert False

def todo_urgency(todo):
    urgencies = {}
    for entry in todo:
        due_date = entry[0]
        _, urgency_name = datetime_urgency(due_date)
        if urgency_name not in urgencies:
            urgencies[urgency_name] = []
        urgencies[urgency_name].append(entry)
    return urgencies

