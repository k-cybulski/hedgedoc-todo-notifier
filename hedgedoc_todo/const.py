from datetime import timedelta

SECTION_WEEKS = 'Weeks'
COLUMN_WEEK = 'Week'
COLUMN_DUE_DATE = 'Due'
COLUMN_WEEK_START = 'Monday'
SECTION_COURSES = 'Courses'
TABLES_KEY = '__tables__'
NO_TASK_STRINGS = {'', '$-$', '-'}
COMPLETE_TASK_STRINGS = [':zap:', ':eyes:', ':sparkles:']
UNFINISHED_TASK_STRINGS = [':boom:']
# how many days to add to a week start to consider all tasks due;
# at 7, the deadline is end of Sunday
TIMEDELTA_WEEK_LENGTH = timedelta(7)

TIMEDELTA_NORMAL = timedelta(7)
TIMEDELTA_URGENT = timedelta(3)
TIMEDELTA_OVERDUE = timedelta(0)
URGENCY_LEVEL_CHILL = (-1, 'Chill', None)
URGENCY_LEVELS = [(0, 'Normal', TIMEDELTA_NORMAL), (1, 'Urgent', TIMEDELTA_URGENT), (2, 'Overdue', TIMEDELTA_OVERDUE)]

HTP_URGENCY_TO_NOTIFY_SEND_URGENCY = {
    'Normal': 'normal',
    'Chill': 'low',
    'Urgent': 'critical',
    'Overdue': 'critical'
}
HTP_URGENCY_TO_NOTIFY_SEND_TIMEOUT = {
    'Normal': '30000',
    'Chill': '10000',
    'Urgent': '60000',
    'Overdue': '60000'
}
NOTIFY_SEND_CATEGORY = 'hedgedoc_todo_parser'
NOTIFY_SEND_SLEEP_TIME = 0.5
