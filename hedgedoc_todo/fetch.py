import posixpath

import requests

HEDGEDOC_DOWNLOAD_COMMAND = "download"

def _download_url(config):
    return posixpath.join(config['Server'], config['Note'],
            HEDGEDOC_DOWNLOAD_COMMAND)

def fetch_markdown(config):
    url = _download_url(config)
    response = requests.get(url)
    return response.text
