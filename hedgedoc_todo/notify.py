# This requires notify-send to be installed
from time import sleep
from datetime import datetime

from sh import notify_send

from .markdown import todo_urgency
from .const import (HTP_URGENCY_TO_NOTIFY_SEND_URGENCY, 
        HTP_URGENCY_TO_NOTIFY_SEND_TIMEOUT,
        NOTIFY_SEND_CATEGORY,
        NOTIFY_SEND_SLEEP_TIME,
        URGENCY_LEVEL_CHILL,
        URGENCY_LEVELS, URGENCY_LEVEL_CHILL)

def todo_to_per_course_list(todo):
    courses = {}
    for entry in todo:
        if entry[1] not in courses:
            courses[entry[1]] = []
        courses[entry[1]].append(entry)
    return courses

def todo_to_nice_list(todo):
    out_str = ""
    for (due_date, course, task_type, task_text) in todo:
        date_str = datetime.strftime(due_date, "%B %d")
        out_str += f"{date_str} {task_type}"
        if task_text != '':
            out_str += f": {task_text}"
        out_str += "\n"
    return out_str

def notify_todo(todo):
    urgencies = todo_urgency(todo)
    for urgency_level in list(reversed(URGENCY_LEVELS)) + [URGENCY_LEVEL_CHILL]:
        if urgency_level[1] not in urgencies:
            continue
        urgency = urgency_level[1]
        todo = urgencies[urgency]
        notify_send_urgency = HTP_URGENCY_TO_NOTIFY_SEND_URGENCY[urgency]
        notify_send_timeout = HTP_URGENCY_TO_NOTIFY_SEND_TIMEOUT[urgency]
        courses = todo_to_per_course_list(todo)
        for course, todo_course in courses.items():
            notify_send('-u', notify_send_urgency, 
                    '-c', NOTIFY_SEND_CATEGORY,
                    '-t', notify_send_timeout,
                    f"{urgency}: {course}", todo_to_nice_list(todo_course))
            sleep(NOTIFY_SEND_SLEEP_TIME)
