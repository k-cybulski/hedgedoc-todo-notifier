from configparser import ConfigParser
from os.path import expanduser, expandvars

DEFAULT_CONFIG_LOCATIONS = ['$PWD/.hedgedoc_todo', '~/.config/hedgedoc_todo']

def get_default_config():
    config = ConfigParser()
    for location in DEFAULT_CONFIG_LOCATIONS:
        expanded = expanduser(location)
        expanded = expandvars(expanded)
        config.read(expanded)
    # should validate
    return config['Main']
