from hedgedoc_todo.config import get_default_config
from hedgedoc_todo.fetch import fetch_markdown
from hedgedoc_todo.markdown import (parse_to_entries, Plan, plan_to_todo)
from hedgedoc_todo.notify import notify_todo, notify_send

try:
    config = get_default_config()
    markdown = fetch_markdown(config)
    entries = parse_to_entries(markdown)
    plan = Plan.from_entries(entries)
    todo = plan_to_todo(plan)

    notify_todo(todo)
except Exception as e:
    notify_send('-t', '10000', "HedgeDoc todo error", f"{type(e).__name__}: {e}")
