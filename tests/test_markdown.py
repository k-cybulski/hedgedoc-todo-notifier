import pytest

from hedgedoc_todo.markdown import parse_to_entries, Table

def test_tables_with_whitespace():
    markdown = """
| Week | Monday |
|------|--------|
| 1	| 2022-09-19 | 
| 2	| 2022-09-26 | 
| 12 | 2022-12-05 |
| 13 | 2022-12-12 |
| 14 | 2022-12-19 |
"""
    entries = parse_to_entries(markdown)
    assert len(entries) == 1
    assert entries[0] == Table(
            ['Week', 'Monday'],
            {'Week': ['1', '2', '12', '13', '14'], 
             'Monday': ['2022-09-19', '2022-09-26', '2022-12-05', '2022-12-12', '2022-12-19']}
            )
