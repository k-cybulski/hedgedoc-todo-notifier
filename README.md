# HedgeDoc TODO notifier
A little utility for parsing a list of tasks from various courses written in HedgeDoc markdown documents, and compiling them into a list based on urgency, which can then be showed as desktop notifications.

<div align="center">

![](example.png)
</div>

The above image shows how notifications from the example described below look like.

## Example
A config file in one of the following paths is necessary:
 - `.hedgedoc_todo` in the current working directory 
 - `~/.config/hedgedoc_todo`

This config file includes the path to a hedgedoc instance, and has the following format:
```ini
[Main]
Server = https://md.example.test
Note = cool-university-study-plan-2022
```

Then, the document `https://md.example.test/cool-university-study-plan-2022` ought to follow the following rules:
 - There needs to be a section `## Weeks` which has a Weeks table that maps the number of each week in the term to the date of the corresponding Monday.
 - There needs to be a section `## Courses`, which includes subsections `### <course name>` that can then contain tables. These tables can have two different formats:
   - They can have a column `Week`, in which case this column must have integer values. In this case, each row means "tasks to finish by the end of this week".
   - They can have a column `Due`, in which case it's a date/time string. In this case, each row means "this is the deadline for the task"
 - A task is marked as complete if it includes `:zap:` or `:eyes:`.

An example of a valid markdown study plan is here
```md
# ETHZ study plan 2022 Spring

## Weeks

|Week| Monday     |
|----|------------|
|1   | 2022-02-21 |
|2   | 2022-02-28 |
|3   | 2022-03-07 |
|4   | 2022-03-14 |
|5   | 2022-03-21 |
|6   | 2022-03-28 |

## Courses

### [Computational Statistics](https://moodle-app2.let.ethz.ch/course/view.php?id=16621)

|Week|Exercises             |Anki Lectures|Anki Exercises|
|----|----------------------|-------------|--------------|
|1   | R tutorial :zap:     | :zap:       | :zap:        |
|2   | Series 1 :zap:       | :zap:       | :zap:        |
|3   | Series 2 :boom:      | :zap:       | :boom:       |

### [Empirical Process Theory](https://stat.ethz.ch/lectures/ss22/EPT.php#course_materials)

|Week|Anki Lectures|
|----|--------------------|
|1   | 23.02.2022 :zap:   |
|2   | 02.03.2022 :eyes:  |
|3   |                    |
|4   |                    |

### [Optimization for Data Science](https://www.ti.inf.ethz.ch/ew/courses/ODS22/index.html)

|Week|Assignments |Exercises        |Anki Lectures|Anki Exercises|
|----|------------|-----------------|-------------|--------------|
|1   | $-$        | Set 1 :zap:     | :zap:       | :zap:        |
|2   | $-$        | Set 2 :boom:    | :boom:      | :boom:       |
|3   | GA 1       |                 |             |              |
|4   | $-$        |                 |             |              |
|5   | $-$        |                 |             |              |


|Due              |Assignment|
|-----------------|----------|
|2022-03-09 16:00 | Nesterov |
|2022-03-23 16:00 | Adam     |
```

If all is done, and `notify.py` is executed, then you should get a nice stream of notifications listing what you should work on.
